# Base
FROM python:3.6-alpine

RUN apk add --no-cache bash

# Istall deps
COPY ./requirements.txt /
RUN pip3 install -r requirements.txt 


# Install dist package sortapp
COPY ./dist/sortapp-1.0.0.tar.gz /

RUN pip3 install sortapp-1.0.0.tar.gz 

#
#RUN rm sortapp-1.0.0.tar.gz
#RUN rm requirements.txt
#RUN rm -r /root/.cache

### Alternative ###
# Create a working directory and Bundle app source
# WORKDIR /sortapp
# COPY src/sortapp /sortapp

# Copy all subfolder
#ADD . /

# Autorun
# CMD [ "python3", "./sortapp.py" ]
